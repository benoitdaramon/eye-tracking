 /*
  * Eye tracking using  AD5232 
  * 
  * GND - GND
  * 3.3V - 3.3V
  * OUTPUT - A0
  * LO- -  Digital Pin 11
  * LO+ - Digital Pin 10
  */

int sensorValue = 0;
int i = 0;
int avg = 0;
int sum = 0;
void setup() {
  Serial.begin(9600);
  pinMode(10, INPUT); // Setup for leads off detection LO +
  pinMode(11, INPUT); // Setup for leads off detection LO -

}

void loop() {
  
  if((digitalRead(10) == 1)||(digitalRead(11) == 1)){
    Serial.println('!');
  }
  else{
      sensorValue = analogRead(A0);
      display();
  }
  delay(1);
}

void display(){
      sum = sum + sensorValue;
      i++;
      if (i == 10) {
        avg=sum/10;
        Serial.println(avg);
        i = 0;
        sum = 0;
      }
      
  
}

